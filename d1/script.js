//Get post data
//promise-holds the eventual result of an asynchronous operation
//promise states-pending, resolved or fulfilled, rejected
//Asynchronous Operation-any js operation that would take time to complete. Example would be network request

//How to consume promises.
fetch('https://jsonplaceholder.typicode.com/posts')
    .then(response => response.json())
    .then(data => showPosts(data))
    // .then(data => console.log(data))
    .catch(err => console.log(err))


// const p = new Promise((resolve, reject) => {
//     //asynchronous operation
//     // resolve(1)
//     reject(new Error('message'))
// })

// //console.log(p)
// p
//     .then(result => console.log('Result:', result))
//     .catch(err => console.log('Error:', err.message))

const showPosts = (posts) => {
    let postEntries = ''
    posts.forEach((post) => {
        postEntries += `
            <div id='post-${post.id}'>
                <h3 id="post-title-${post.id}">${post.title}</h3>
                <p id="post-body-${post.id}">${post.body}</p>
                <button onclick="editPost(${post.id})">Edit</button>
                <button onclick="deletePost(${post.id})">Delete</button>
            </div>
        `
    })
    document.querySelector('#div-post-entries').innerHTML = postEntries;
}
//Delete Post
const deletePost = (id) => {
    fetch(`https://jsonplaceholder.typicode.com/posts/${id}`, { method: 'DELETE' });
    document.querySelector(`#post-${id}`).remove();
}
// alert('Delete this post' + id)


//Edit Post

const editPost = (id) => {
    //alert('Edit this post' + id)
    let title = document.querySelector(`#post-title-${id}`).innerHTML
    let body = document.querySelector(`#post-body-${id}`).innerHTML

    document.getElementById("txt-edit-id").value = id
    document.getElementById("txt-edit-title").value = title
    document.getElementById("txt-edit-body").value = body
    document.querySelector("#btn-submit-update").removeAttribute('disabled')


    //Update Post
    let updateForm = document.querySelector('#form-edit-post')
    updateForm.addEventListener('submit', (e) => {
        e.preventDefault()
        // console.log(id)
        fetch('https://jsonplaceholder.typicode.com/posts/1', {
            method: 'PUT',
            body: JSON.stringify({
                id: id,
                title: document.querySelector('#txt-edit-title').value,
                body: document.querySelector('#txt-edit-body').value,
                userId: 1
            }),
            headers: { 'content-type': 'application/json; charset=UTF-8' }
        })
            .then((response) => response.json())
            .then((data) => {
                console.log(data);
                alert('Post Updated')
            })
    })
}


//Add Post

let addPostForm = document.querySelector('#form-add-post')
addPostForm.addEventListener('submit', (e) => {
    e.preventDefault();

    fetch('https://jsonplaceholder.typicode.com/posts', {
        method: 'POST',
        body: JSON.stringify({
            title: document.getElementById('txt-title').value,
            body: document.getElementById('txt-body').value,
            userId: 1

        }),
        headers: { 'Content-type': 'application/json;charset=UTF-8' }

    })
        .then(response => response.json())
        .then(data => {
            console.log(data)
            alert('Post added')
            document.querySelector('#txt-title').value = ''
            document.querySelector('#txt-body').value = ''
        })
})
